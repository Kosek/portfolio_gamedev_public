# Portfolio

* Name: Tomasz Kosek
* E-mail: kosek.tomasz@gmail.com

## Development of Nanzou: The Divine Court

## https://store.steampowered.com/app/1390760/Nanzou_The_Divine_Court/

<figure style="text-align:center">
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/15_nanzour_the_divine_court/Nanzou_Key_art_logo.png' alt='missing'/>
</figure>
<br>

## Initial Development of Dying Light 2 Stay Human Bloody Ties DLC

<figure style="text-align:center">
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/14_dying_light_2_bloody_ties/DyingLight2StayHumanBloodyTiesTechlandDLC.jpeg' alt='missing'/>
</figure>
<br>

## Cashier

## https://play.google.com/store/apps/details?id=com.rotaku.cashier

<figure style="text-align:center">
    <figcaption>Cashier screenshots:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/13_cashier/screen_16_9_menu.jpg' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/13_cashier/screen_16_9_map.jpg' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/13_cashier/screen_16_9_conveyor.jpg' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/13_cashier/screen_16_9_shop.jpg' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/13_cashier/screen_16_9_mobile.jpg' alt='missing'/>
</figure>
<br>

## Zombie Crowds in Dying Light 2: Stay Human - Talk at Digital Dragons 2022

## https://www.youtube.com/watch?v=6GgtmMSlBfU

<figure style="text-align:center">
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/12_talk_at_digital_dragons_2022/digital_dragons_2022_zombie_crowds_in_dying_light_2_stay_human.png' alt='missing'/>
</figure>
<br>

## Dying Light 2

## https://dyinglightgame.com/

<figure style="text-align:center">
    <figcaption>Dying Light 2 screenshots:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/11_dying_light_2/DyingLight2_screen_08.jpg' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/11_dying_light_2/DyingLight2_screen_02.jpg' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/11_dying_light_2/DyingLight2_screen_03.jpg' alt='missing'/>
</figure>
<br>

## Multithreaded renderer

## https://gitlab.com/Kosek/multi_threaded_renderer

<figure style="text-align:center">
    <figcaption>Sample screeshot from the Multithreaded Renderer demo:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/10_multithreaded_renderer/multithreaded_renderer_3.jpg' alt='missing'/>
</figure>
<br>


## Time-location planning software � TILOS (https://www.app-consultoria.com/tilos-time-location-for-infrastructure-projects/)

* Drawing various figures on time-location charts.
* Transformation of time-location coordinates to pixel coordinates.
* Math calculations to draw instanced figures based on template figure.

<figure style="text-align:center">
    <figcaption>Sample time-location chart in Tilos:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/8_tilos/time_location.png' alt='missing'/>
</figure>
<br>


## Triangle based collision detection

* Indexing scene using octree.
* Math calculations to correct colliding objects.
* Querying triangles via view frustum applied to octree.

<figure style="text-align:center">
    <figcaption>Triangle based collision detection:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/6_physics/collision_detection_1.png' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <figcaption>Same scene (as above) without wireframe:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/6_physics/collision_detection_2.png' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <figcaption>Same scene (as above) with indexing octree visible:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/6_physics/collision_detection_3.png' alt='missing'/>
</figure>
<br>


## Rendering of shadow casting

* Shadow map based shadow casting.
* Anti-aliasing of shadow.

<figure style="text-align:center">
    <figcaption>Shadow map based shadow casting with shadow anti-aliasing:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/5_shadows/aa_shadow_1.png' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/5_shadows/aa_shadow_2.png' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/5_shadows/aa_shadow_3.png' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/5_shadows/aa_shadow_4.png' alt='missing'/>
</figure>
<br>


## Rendering of 3ds Max animated 3D model

* Animation of 3D model in 3ds Max.
* Export/import of 3D model using FBX format (using FBX SDK).
* Normal vector based shading.
* Heightmap based terrain generation.
* Model animation based on sequence of pre baked keyframe transformation matrices.
* Model animation played based on keyboard input.

<figure style="text-align:center">
    <figcaption>Animated 3D model visible from different angles:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/4_ball/ball_1.JPG' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/4_ball/ball_2.JPG' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/4_ball/ball_3.JPG' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <figcaption>Sample frames of animation of 3D model (3ds Max):</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/4_ball/ball_run_1.png' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/4_ball/ball_run_2.png' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/4_ball/ball_run_3.png' alt='missing'/>
</figure>
<br>


## Steganography in Computer Games (Master Thesis Topic)

* Dynamic information hiding using Pixel Shader.
* Dynamic information hiding in real time animation.
* Extraction of watermarks from textures based on screenshots taken from rendered scene.

<figure style="text-align:center">
    <figcaption>Scene overview:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/3_masters_thesis/lsb_overview.png' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <figcaption>Objects with hidden information which is not visible:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/3_masters_thesis/lsb_zoom.png' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <figcaption>Objects with hidden information which is visible:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/3_masters_thesis/lsb_zoom_with_hidden_message.png' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <figcaption>Dynamic animation:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/3_masters_thesis/matrix_code.png' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <figcaption>Secret message highlighted:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/3_masters_thesis/matrix_code_with_hidden_message_visible.png' alt='missing'/>
</figure>
<br>


## 2D Game Editor

* Real time scene edit mode.
* Assets handling (loading and compilation).
* Depth layers.
* Sprite animation definitions based on state machines.
* Usage of Box2D as external physics simulator.
* Game compilation into executable.
* Plug-in based mechanism to extend editor's game specific features.

<figure style="text-align:center">
    <figcaption>Scene editing in editor:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/7_editor/scr1.png' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <figcaption>Compiled game scene view:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/7_editor/scr2.png' alt='missing'/>
</figure>
<br>


## Simple 3D Editor

* Space indexing via octree.
* Ray based object picking in 3D space.

<figure style="text-align:center">
    <figcaption>Sample object in the editor's view:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/9_editor_3d/geditor_3d.png' alt='missing'/>
</figure>
<br>


## The Witcher Game Gallery (Adobe Flash)

* Graphics prepared using Adobe Photo Shop.
* Particle effects scripted using Adobe Flash Script.
* Procedurally generated round volume icon.

<figure style="text-align:center">
    <figcaption>The Witcher Gallery game screenshots:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/2_the_witcher_gallery/gallery_1.JPG' alt='missing'/>
</figure>
<br>
<figure style="text-align:center">
    <figcaption>The Witcher Gallery menu navigator and background music theme selection menu:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/2_the_witcher_gallery/gallery_2.JPG' alt='missing'/>
</figure>
<br>


## 3ds Max 3D Animation of Rubik's Cube solving

* Creation of Rubik's Cube 3D model.
* Animation of Rubik's Cube being solved.
* Dynamic camera movement during the animation.

<figure>
    <figcaption>3ds Max's 174 frame of animation:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/1_Rubiks_Cube/3ds_Max_screenshot.JPG' alt='missing'/>
</figure>
<br>
<figure>
    <figcaption>3ds Max's 630 frame of animation:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/1_Rubiks_Cube/3ds_Max_screenshot_2.JPG' alt='missing'/>
</figure>
<br>
<figure>
    <figcaption>Beginning of animation footage:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/1_Rubiks_Cube/movie_screenshot_1.jpg' alt='missing'/>
</figure>
<br>
<figure>
    <figcaption>End of animation footage:</figcaption>
    <br>
    <img src='https://gitlab.com/Kosek/portfolio_gamedev_public/raw/master/assets/1_Rubiks_Cube/movie_screenshot_2.jpg' alt='missing'/>
</figure>
<br>